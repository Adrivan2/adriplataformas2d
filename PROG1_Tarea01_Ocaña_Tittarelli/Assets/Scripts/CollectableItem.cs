﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase encargada del objeto coleccionable
/// </summary>
public class CollectableItem : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        // Comprueba si es el jugador el que entra en contacto para destruir el coleccionable
        if (other.CompareTag("Player")) {
            GameManager.GotCollectable();
            Destroy(gameObject);
        }
    }
}
