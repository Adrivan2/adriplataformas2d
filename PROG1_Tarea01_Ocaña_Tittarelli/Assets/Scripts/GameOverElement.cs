﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase para gestionar los elementos que provocan el fin de partida
/// </summary>
public class GameOverElement : MonoBehaviour {

    #region Funciones de Unity

    private void OnCollisionEnter(Collision collision) {
        // Comprueba si fue el jugador quien entró en colisión, para provocar el fin de juego
        if (collision.collider.CompareTag("Player")) {
            GameManager.GameOver();
        }
    }

    #endregion
}
