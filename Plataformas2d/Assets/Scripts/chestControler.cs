using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chestControler : MonoBehaviour
{
    private Animator anim;
    private bool playerin;
    // Start is called before the first frame update
    void Start()
    {
        playerin = false;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("playerin", playerin);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerin = false;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerin = true;
        }
    }
}
