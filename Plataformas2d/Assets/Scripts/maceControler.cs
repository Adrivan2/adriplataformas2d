using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class maceControler : MonoBehaviour
{
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 5, LayerMask.GetMask("Player"));
        if ((hit.collider != null) && (hit.collider.CompareTag("Player")))
        {
            anim.SetTrigger("detected");
        }
    }
}
